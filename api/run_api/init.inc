<?php
/** LIS Initialization Library
 * 
 *  PHP version 5 
 *
 * @category ProjectBootstrap
 * @package  
 * @author   Sumit <sumit.jain.knit@gmail.com>
 * @license  
 * @link      
 *
 **/

// System Constants and Setup
date_default_timezone_set('Asia/Calcutta');
define('WHATUPSTARTUP_APPLICATION_ROOT', dirname(__FILE__).'/');
define('WHATUPSTARTUP_APPLICATION_NAME', 'WHATUPSTARTUP');

//Runtime PHP Behaviour
//ini_set('display_errors', 'off');
//ini_set('log_errors', 'on');
//ini_set('error_level', E_ALL);
//ini_set('session.save_path', '/var/lib/php/session');


// all libraries in the project
require WHATUPSTARTUP_APPLICATION_ROOT.'constants.inc';
require WHATUPSTARTUP_APPLICATION_ROOT.'logger.inc';
require WHATUPSTARTUP_APPLICATION_ROOT.'../db_api/dblayer.inc';
require WHATUPSTARTUP_APPLICATION_ROOT.'../db_api/generatequery.inc';
require WHATUPSTARTUP_APPLICATION_ROOT.'../db_api/middleware.inc';
require WHATUPSTARTUP_APPLICATION_ROOT.'../db_api/querydriver.inc';
require WHATUPSTARTUP_APPLICATION_ROOT.'utils.inc';



// DONT THINK OF PUTTING ENDING TAG BELOW
