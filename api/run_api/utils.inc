<?php

/** 
 * All the utilities
 * 
 * PHP version 5 
 *
 * @category 
 * @package  
 * @author   Sumit <sumit.jain.knit@gmail.com>
 * @license  
 * @link     
 *
 **/

/**
 * Sanitize given filename, replace bad chars with spaces
 *
 * @param string $file filename to be sanitizedd
 *
 * @return string new filename
 **/
function sanitizeFileName($file)
{
    return preg_replace('/[\\/\:\*\?\"\<\>\|]+/', ' ', str_replace('\\', ' ', $file));
}

/*
    Example for sanitize
    $files = array(
    'hello:.jpg' => 'hello',
    'h:e:l:l:o:.txt' => 'hello',
    '\h:e:l:l:o:.txt' => 'hello',
    '\h:e:l:l:o:.txt' => 'hello',
    '//h:e:l:l:o:.txt' => 'hello',
    '//?h?e?l<l>o:.txt' => 'hello',
    );
    foreach($files as $file => $sanitized) {
    echo sanitizeFileName($file)."\n";
    }
*/

/**
 * Sanitize given filename, replace bad chars with spaces
 *
 * @param string $dateTime Date and Time string
 * @param string $format   Format in which we want return value
 * @param string $dbTz     Timezone in which $dateTime is stored in database
 * @param string $userTz   Timezone in which we want the datetime to new converted as
 *
 * @return string time in choosen $format
 **/

function dbToUIDateTime($dateTime, $format, $dbTz, $userTz)
{
    $d   = new DateTime($dateTime, new DateTimeZone($dbTz));
    if ($dbTz !== $userTz) {
        $d->setTimezone(new DateTimeZone($userTz));
    }
    return $d->format($format);
}

/*
    Example for dbToUIDateTime
    $date = '2012-02-17';
    $time = '18:16:57';
    $dateTime = "$date $time";
    $dbTz = "Asia/Kolkata";
    $uTz  = "GMT";
    $u1Tz = "America/New_York";

    echo "Original($dbTz) : $dateTime\n";
    echo "New ($uTz) : ".dbToUIDateTime($dateTime, "U", $dbTz, $uTz)."\n";
    echo "New ($uTz) : ".dbToUIDateTime($dateTime, "Y-m-d H:i:s", $dbTz, $uTz)."\n";
    echo "New ($u1Tz) : ".dbToUIDateTime($dateTime, "Y-m-d H:i:s", $dbTz, $u1Tz)."\n";
*/

/**
 * Decode the contents of given JSON File
 *
 * @param string  $file      File Path which needs decoded
 * @param boolean $wantArray True if we return an array, False if we want object instead
 *
 * @return array or object with decoded json contents
 **/
function My_Json_Decode_file($file, $wantArray=false)
{
    $data = json_decode(file_get_contents($file), $wantArray);
    $jsonerror = null;
    if (isset($jsonerror) === true) {
        throw new Exception($jsonerror);
    }
    return $data;
}

/**
 * Create a HTML Element
 *
 * @param string $tag     HTML Tag name
 * @param string $opts    List of key value pairs for the element
 * @param string $content Data that goes inside the element
 *
 * @return string with html markup
 *
 **/
function htmlEL($tag, $opts=array(), $content='')
{
    $self_closing_tags = array(
        // Via: http://www.w3.org/TR/html5/syntax.html#void-elements
        'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'
    );
    $ltag  = strtolower($tag);
    $partx = '';
    $data  = array();

    // Take care of the arguments
    if (is_array($opts) and count($opts) > 0) {
        $parts = array();
        foreach ($opts as $key => $value) {
            $parts[] = join('=', array($key, '"'.addslashes($value).'"'));
        }
        $partx = ' '.join(' ', $parts);
    }

    // Take care of the tag and its content
    if (in_array($ltag, $self_closing_tags) === true) {
        $data[] = '<'.$ltag.$partx.' />';
    } else {
        if (is_array($content) and count($content) > 0) {
            foreach ($content as $idx => $call) {
                if (is_array($call) and count($call) === 3) {
                    $data[] = '<'.$ltag.$partx.'>'.htmlEL($call[0], $call[1], $call[2]).'</'.$ltag.'>';
                } else if (is_array($call) === true) {
                    $data[] = '<'.$ltag.$partx.'>'.join('', $call).'</'.$ltag.'>';
                } else {
                    $data[] = '<'.$ltag.$partx.'>'.$call.'</'.$ltag.'>';
                }
            }
        } else {
            $data[] = '<'.$ltag.$partx.'>'.$content.'</'.$ltag.'>';
        }
    }

    return join('', $data)."\n";

}

/*
    Example for htmlEL
    echo htmlEL(
    'div',
    array('class'=>"control-group"),
    array(
        array(
            'label',
            null,
            'System Name :',
        ),
        array(
            'div',
            array('class'=>'controls'),
            array(
                'label',
                null,
                $systemdetails['data'][0]['name'],
            )
        )
    )
    );

    $style = array('href' => '1.2.3.php?something&value=something&value=?      ', 'class' => 'something');
    echo htmlEl('a', $style, 'hello');
    $loadTags = json_decode('
    [
    "fieldset",
    null,
    [
        [
            @@replace.this.tag@@
        ],
        [
        ]
    ]
    ]');
    echo processData($loadTAgs, array('@@@replace.this.tag@@@' => 'my name is this'));
    $k
    echo htmlEL('input', array('type' => 'text'), 'text')."\n";
    echo htmlEL('input', array('type' => 'text', 'disabled' => 'disabled'), 'text')."\n";
    echo htmlEL('link', array('rel' => 'stylesheet', 'src' => 'something.css'), 'text')."\n";
    echo "\n";
    echo htmlEL(
    'fieldset',
    null,
    array(
        array('legend', null, 'This is legend text'),
        array(
            'div',
            null,
            array(
                array(
                    'div',
                    null,
                    null,
                )
            )
        )
    )
    );
    echo htmlEL(
    'div',
    null,
    array(
        array(
            'div',
            null,
            'This is cool div'
        )
    ),
    'text'
    )."\n";

    will output these lines

    <input type="text" />
    <input type="text" disabled="disabled" />
    <link rel="stylesheet" src="something.css" />

*/

/**
 * get_browser_details Return the true or false according to browser and version
 *      
 * @return return true and false .
 */
function Get_Browser_details()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $version = '';
    //  get the name of the useragent seperately
    if ((preg_match('/MSIE/i', $u_agent) === 1) && (preg_match('/Opera/i', $u_agent) === 0)) {
        $bname = 'Internet Explorer';
        $ub = 'MSIE';
    } else if (preg_match('/Firefox/i', $u_agent) === 1) {
        $bname = 'Mozilla Firefox';
        $ub = 'Firefox';
    } else if (preg_match('/Chrome/i', $u_agent) === 1) {
        $bname = 'Google Chrome';
        $ub = 'Chrome';
    } else if (preg_match('/Safari/i', $u_agent) === 1) {
        $bname = 'Apple Safari';
        $ub = 'Safari';
    } else if (preg_match('/Opera/i', $u_agent) === 1) {
        $bname = 'Opera';
        $ub = 'Opera';
    } else if (preg_match('/Netscape/i', $u_agent) === 1) {
        $bname = 'Netscape';
        $ub = 'Netscape';
    }
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>'.join('|', $known).')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (preg_match_all($pattern, $u_agent, $matches) === 0) {
        // we have no matching number just continue
    }
    // see how many we have
    $i = count($matches['browser']);
    if ($i !== 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, 'Version') < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }
    $ver  = explode('.', $version);
    $ver1 = $ver[0].'.'.$ver[1];
    if ($ub === 'MSIE' && $ver1 < 9.0 ) {
        return true;
    } else {
        return false;
    }
}

/**
 * send_JSON Send JSON Content to the browser
 *
 * @param mixed $obj can be any serializable php object
 *      
 * @return nothing
 */

function Send_json($obj)
{
    header('Content-type: application/json');
    echo json_encode($obj);
    exit;
}






/**
 * Function to translate a String
 *
 * @param string $string string to translate
 *
 * @return translated string
 */
function _translate($string)
{
    return $string;
}


/**
 * Get Remote IP address
 *
 * @return remote ip address
 */

function getRemoteIP()
{
    $ip = $_SERVER['REMOTE_ADDR'];
    return $ip;
}

/**
* get_browser_details Return the true or false according to browser and version
*      
* @return return true and false .
*/
function Get_Browser_details2()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    return $u_agent;
}



/**
 * function handles the change of a multiple drop down category
 *
 * @param array $getArray array of all categories selected. got through $_GET or $_POST
 *
 * @return nothing
 */
 
 
 
 
 
 
 
 /**
 * Create Server side pagination
 *
 * @param number  $url      URL to be used in the link
 * @param number  $total    Total number of records in the system
 * @param number  $curPage  current page that is being shown to the user
 * @param number  $perPage  Number of records to be shown per page
 * @param number  $maxPages maximum number of pages to be shown in the navigation bar
 * @param string  $class    classification to be used when more than one table is present on the same page
 * @param string  $xcss     extra css classnames
 * @param array   $qParams  Any extra query parameters
 * @param string  $eltype   Use custom element type
 * @param boolean $useid    True if we want an id to be used for elements
 *
 * @return paginated list of elements
 **/
function createPagination($url, $total, $curPage, $perPage, $maxPages, $show=true, $class='', $xcss='', $qParams=array(), $eltype='a', $useid=false)
{
    $curPage = intval($curPage);
    $ul[] = '<div class=" '.$xcss.'"><ul id="pagination" class="pagination">';
    $r  = getPages($total, $curPage, $perPage, $maxPages);
    if (count($r) <= 0) {
        return '';
    }
    $links = 0;
	$ul[] = '<li><a class ="0" href="#">&larr;</a></li>';
  
    foreach ($r as $pageID) {
        $links++;
        $opts = array(
            $class.'curPage' => $pageID,
            $class.'perPage'  => $perPage,
            $class.'maxPages'   => $maxPages,
        );
        foreach ($qParams as $key => $value) {
            $opts[$key] = $value;
        }
        $idval = 'page_'.$links; // value of the id
        if (preg_match('/^\*(\d+)$/', $pageID, $matches) === 1) {
            $opts[$class.'curPage'] = $matches[1];
            if ($eltype === 'a') {
                $nurl = $url.'?'.http_build_query($opts);
                if ($useid === true) {
                    $ul[] = '<li class="active"><a class ="'.($matches[1]-1).'" href="'.$nurl.'">'.$matches[1].'</a></li>';
                } else {
                    $ul[] = '<li class="active"><a class ="'.($matches[1]-1).'" id="'.$idval.'" href="'.$nurl.'">'.$matches[1].'</a></li>';
                }
            } else {
                if ($useid === true) {
                    $ul[] = '<li class="active"><span class="paginate" id="'.$idval.'">'.$matches[1].'</span></li>';
                } else {
                    $ul[] = '<li class="active"><span class="paginate">'.$matches[1].'</span></li>';
                }
            }
        } else {
            $nurl = $url.'?'.http_build_query($opts);
            if ($eltype === 'a') {
                if ($useid === true) {
                    $ul[] = '<li><a class ="'.($pageID-1).'" id="'.$idval.'" href="'.$nurl.'">'.$pageID.'</a></li>';
                } else {
                    $ul[] = '<li><a class ="'.($pageID-1).'" href="'.$nurl.'">'.$pageID.'</a></li>';
                }
            } else {
                if ($useid === true) {
                    $ul[] = '<li><span class="paginate" id="'.$idval.'">'.$pageID.'</span></li>';
                } else {
                    $ul[] = '<li><span class="paginate">'.$pageID.'</span></li>';
                }
            }
        }
    }//return($links-1);
	$last = intval($total/$perPage);
	$ul[] = '<li><a class ="'.($links-1).'" href="'.$url.'?'.http_build_query($opts).'">&rarr;</a></li>';
    $ul[] = '</ul>';
    $b    = (($perPage * ($curPage - 1)) + 1);
    $e    = min(($perPage * $curPage), $total);
	if ($show === true) {
		$str  = sprintf('Showing %d - %d of %d Records', $b, $e, $total);
		$ul[] = '<div class="paginationsummary"><span>'.$str.'</span></div>';
	}
    $ul[] = '</div>';
    return join('', $ul);
}

/**
 * Find the pagination sequence
 *
 * @param number $total    Total number of records in the system
 * @param number $curPage  current page that is being shown to the user
 * @param number $perPage  Number of records to be shown per page
 * @param number $maxPages maximum number of pages to be shown in the navigation bar
 *
 * @return array of elements with paginated sequence
 **/
function getPages($total, $curPage, $perPage, $maxPages)
{
    $praw     = $total / $perPage;
    $pages    = ($praw > intval($praw)) ? intval($praw) + 1 : $praw;
    $resp     = array();

    if ($total < ($perPage * $maxPages)) {
        $maxPages  = $pages;
        $startPage = 1;
    }

    // Cap the Min page at 1 though user passes lower value
    if ($curPage < 1) {
        $curPage = 1;
    }
    // Cap the Max page at totalPages though user passes higher value
    if ($pages < $curPage) {
        $curPage = $pages;
    }

    // Have a local variable to figure out where do we want to start
    $startPage = $curPage;

    // Keep the starting Page always at middle based on how many more rows are possible
    if ((- $curPage + intval($maxPages / 2) > 0) and ($curPage - intval($maxPages / 2) <= $pages)) {
        if ($curPage - intval(($maxPages / 2)) > 0) {
            $startPage = - $curPage + intval(($maxPages / 2));
        } else {
            $startPage = max(1, $curPage - intval(($maxPages / 2)));
        }
    } else if (- $curPage + intval($maxPages / 2) > 0) {
        $startPage = (-$curPage + intval(($maxPages / 2)));
    } else if ($curPage - intval($maxPages / 2) <= $pages) {
        $startPage = ($curPage - intval(($maxPages / 2)));
    } else {
        $startPage = $curPage;
    }

    // Make sure startPage > 0
    $startPage = ($startPage <= 0) ? 1 : $startPage;

    // Figure out the total number of iterations we have to make based on starting point
    $parts     = $startPage + $maxPages;

    // If the total parts are more than pages, cap them as well
    if ($parts > $pages) {
        $startPage = $startPage - ($parts - $pages) + 1;
        //$startPage = $startPage - ($parts - $pages);
    } else {
        // This is a no-op
    }

    // Render all the elements happily now
    for ($i = $startPage; $i < $parts; $i++) {
        if ($i > $pages) {
            break;
        }
        if ($i === $curPage) {
            $resp[] = sprintf('*%s', $i);
        } else {
            $resp[] = $i;
        }
    }

    return $resp;
}


function logout_time($field) {
    
	$idletime = SESSION_TIMEOUT_TIME; //after 60 seconds the user gets logged out

	if (time()-$_SESSION[$field]>$idletime){
		session_destroy();
		session_unset();
		header("Location: ../Login/");
		exit;
	} else{
		$_SESSION[$field] = time();
	}
}

/**
 * Retrieve the Headings from sequence
 *
 * @param array $sequence   List of keys used to sequence headers
 * @param array $definition Keys and their display text
 *
 * @return array
 **/
function getHeadings($sequence, $definition)
{
    $response = array();
    foreach ($sequence as $skey) {
        $response[] = $definition[$skey];
    }
    return $response;
}


/**
 * Create HTML Table from given advanced array configuration
 *
 * @param array  $headings   List of table headings
 * @param array  $body       array of arrays
 * @param string $attrs      extra table html tags
 * @param string $em         error message for no rows
 * @param string $trconf     config for row
 * @param string $formatters anonymous functions to format the column
 * @param string $justData   True, if this function should return only data (<tr> only)
 *
 * @return string empty if error, table if good
 **/

function createHTMLTableAdvanced($headings, $body, $attrs='', $em='', $trconf='', $formatters=array(), $justData=false)
{
    if (is_array($headings) === false or is_array($body) === false) {
        return '';
    } else {
        if (is_string($attrs) === true and $attrs !== '') {
            $attrs = ' '.$attrs;
        }
        $table = array();
        if ($justData === false) {
            $table[] = '<table'.$attrs.'>';
            $table[] = '<thead>';
            $head    = '<th>'.join('</th><th>', getHeadings($headings['sequence'], $headings['headmap'])).'</th>';
            $table[] = '<tr>'.$head.'</tr>';
            $table[] = '</thead>';
            $table[] = '<tbody>';
        }
        $rc      = 0;
        $cols    = count($headings['sequence']);
        foreach ($body as $idx => $row) {
            $rdata   = '';
            foreach ($headings['sequence'] as $hkey) {
                $efn    = isset($formatters[$hkey]) === true? $formatters[$hkey] : null;
                // In scenarios when we add custom columns via the configuration
                // we should set a empty string, so that function can work with it
                $row[$hkey] = isset($row[$hkey]) === true? $row[$hkey] : '';
                //$dateobj    = new DateFormat();
                //$dateformat = $dateobj->getUserDateFormat();
                //if (($row[$hkey] !== 'null') && ($row[$hkey] !== date($dateformat, strtotime('1700-01-01')))) {
                    if (isset($efn) === false) {
                        // No formatter
                        $rdata .= '<td>'.$row[$hkey].'</td>';
                    } else if (is_array($efn) === true and count($efn) === 2) {
                        // This is a static member of class
                        $rdata .= '<td>'.call_user_func(array($efn[0], $efn[1]), $row[$hkey], $row).'</td>';
                    } else if (is_array($efn) === true and count($efn) === 1) {
                        // This is a function
                        $rdata .= '<td>'.call_user_func($efn[0], $row[$hkey], $row).'</td>';
                    } else {
                        // This is a closure
                        $rdata .= '<td>'.$efn($row[$hkey], $row).'</td>';
                    }
                //} else {
                //    $rdata .= '<td></td>';
                //}
            }
            $table[] = '<tr'.$trconf.'>'.$rdata.'</tr>';
            $rc++;
        }
        if ($rc === 0) {
            $table[] = '<tr><td colspan='.$cols.'>'.$em.'</td></tr>';
        }
        if ($justData === false) {
            $table[] = '</tbody>';
            $table[] = '</table>';
        }
    }
    return join("\n", $table);
}

/*
    Example

    $config = array(
        'headings' => array(
            'sequence' => array('a', 'b'),
            'headmap'  => array('a' => 'This is a', 'b' => 'This is b', 'c' => 'This is c')
        ),
        'data' => array (
            array('a' => 'Column 1 a', 'b' => 'Column 1 b'),
            array('a' => 'Column 2 a', 'b' => 'Column 2 b'),
            array('a' => 'Column 3 a', 'b' => 'Column 3 b'),
        )
    );
    echo createHTMLTableAdvanced($config['headings'], $config['data']);

*/


function curPageURL() {
 $pageURL = 'http';
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["PHP_SELF"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"];
 }
 return $pageURL;
}

function checkDateTime($data) {
    if (date('Y-m-d', strtotime($data)) == $data) {
        return true;
    } else {
        return false;
    }
}

function parseInput($value) {
	$value = htmlspecialchars($value, ENT_QUOTES, "ISO-8859-1");
	$value = str_replace("\r", "", $value);
	$value = str_replace("\n", "", $value);
	$value = trim($value);
	return $value;
}


function mail_attachment($filename, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
    $file = $filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    if (mail($mailto, $subject, "", $header)) {
        echo "mail send ... OK"; // or use booleans here
    } else {
        echo "mail send ... ERROR!";
    }
}

function Create_zip($source, $destination,$overwrite='')
{
	$zip = new ZipArchive();
	if($zip->open($destination,$overwrite?       ZIPARCHIVE::OVERWRITE:ZIPARCHIVE::CREATE)===TRUE)
	{
		if (is_array($source)) {
			foreach ($source as $data) {
				if (file_exists($data)) {
					$result = $zip->addFile($data);// Add the files to the .zip file
				}
			}
		} else if (is_string($source)){
			if (file_exists($source)) {
				$result = $zip->addFile($source);// Add the files to the .zip file
			}
		}
		
		$zip->close(); // Closing the zip file
	}
	return $result;
}

function displayDate ($datetoshow) {
	$year = date('Y',strtotime($datetoshow));
	$day = date('d',strtotime($datetoshow));
	if (substr($day, 1, 1) === "1") {
		$day = $day . "st";
	} else if (substr($day, 1, 1) === "2") {
		$day = $day . "nd";
	} else if (substr($day, 1, 1) === "3") {
		$day = $day . "rd";
	} else {
		$day = $day . "th";
	}
	$month = substr(date('F',strtotime($datetoshow)), 0, 3);
	$submitDate = $day . " ". $month . " ". $year;
	return $submitDate;
}

function videothumbnail ($videourl) {
	$url = $videourl;
	$urls = parse_url($url);
	//Expect the URL to be http://youtu.be/abcd, where abcd is the video ID
	if ($urls['host'] == 'youtu.be') :
		$imgPath = ltrim($urls['path'],'/');
		//Expect the URL to be http://www.youtube.com/embed/abcd
	elseif (strpos($urls['path'],'embed') == 1) :
		$imgPath = end(explode('/',$urls['path']));
		//Expect the URL to be abcd only
	elseif (strpos($url,'/') === false):
		$imgPath = $url;
		//Expect the URL to be http://www.youtube.com/watch?v=abcd
	else :
		//parse_str($urls['query']);
		//$imgPath = $v;
		$imgPath = "";
	endif;												
	$imgpath = 'http://img.youtube.com/vi/'.$imgPath.'/0.jpg';
	if (strpos($url,'vimeo') == true) {
		sscanf(parse_url($url, PHP_URL_PATH), '/%d', $video_id);
		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_id.php"));
		$imgpath = $hash[0]['thumbnail_large'];  
		//echo "<img class='vimeo_video' video='$video_id' src=$image>";
	}
	return $imgpath;
}