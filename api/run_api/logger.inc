<?php
/** This script is maintaining a log of the errors that occured 
 *  so that debugging is easier. 
 * 
 *  PHP version 5 
 *
 * @category DebugErrorLogger
 * @package  LaboratoryInformationSystem
 * @author   Saurabh Singh <saurabh.singh@mokshadigital.com>
 * @license  http://www.mokshadigital.com/eula.html MOKSHA 1.0
 * @link     http://www.mokshadigital.com 
 *
 * 
 **/

// system is unusable
define('MYLOG_EMERG', 'EMERG');
// action must be taken immediately
define('MYLOG_ALERT', 'ALERT');
// critical conditions
define('MYLOG_CRIT', 'CRIT');
// error conditions
define('MYLOG_ERR', 'ERROR');
// error conditions
define('MYLOG_ERROR', 'ERROR');
// warning conditions
define('MYLOG_WARNING', 'WARN');
// normal, but significant,condition
define('MYLOG_NOTICE', 'NOTICE');
// informational message
define('MYLOG_INFO', 'INFO');
//debug-level message
define('MYLOG_DEBUG', 'DEBUG');

define('VAL_MYLOG_EMERG', 1);
define('VAL_MYLOG_ALERT', 2);
define('VAL_MYLOG_CRIT', 3);
define('VAL_MYLOG_CRITICAL', 3);
define('VAL_MYLOG_ERR', 4);
define('VAL_MYLOG_ERROR', 4);
define('VAL_MYLOG_WARN', 5);
define('VAL_MYLOG_WARNING', 5);
define('VAL_MYLOG_NOTICE', 6);
define('VAL_MYLOG_INFO', 7);
define('VAL_MYLOG_DEBUG', 8);

define('DEFAULT_LOG_LEVEL', VAL_MYLOG_DEBUG);
/**
 * This function stores the error messages in the error_log which can be used
 * for debugging.
 *
 * @param object   $message Error message that is to be put into the debugging 
 *                          error log.
 * @param constant $level   Level of the error i.e. how critical it is.Whether 
 *                          it is high priority or not.
 * @param number   $offset  offset in the backtrace
 *
 * @return null does not return anything.
 **/

function logmymessage($message, $level=MYLOG_DEBUG, $offset=0)
{
    $conf  = constant('DEFAULT_LOG_LEVEL');
    $lval  = constant('VAL_MYLOG_'.$level);
    if ($lval > $conf) {
        return;
    }
    $trace = debug_backtrace();
    if (!isset($trace[$offset])) {
        $offset = 0;
    }
    $file  = $trace[$offset]['file'];
    $f1    = basename($file);
    $f2    = basename(dirname($file));
    $file  = $f2.'/'.$f1;
    $line  = $trace[$offset]['line'];
    $mess  = $message;
    if (is_array($message) or is_object($message)) {
        $mess = json_encode($message);
    }
    error_log($level.': ('.$file.':'.$line.') '.$mess);
}
// DONT THINK OF PUTTING ENDING TAG BELOW
