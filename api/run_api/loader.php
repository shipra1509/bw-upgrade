<?php
/** 
 * Compile the list of all JS/CSS resources for a given page
 * 
 * PHP version 5 
 *
 * @category Loader 
 * @package  RadiologyInformationSystem
 * @author   Naresh <naresh@mokshadigital.com>
 * @license  http://www.mokshadigital.com/eula.html MOKSHA 1.0
 * @link     http://www.mokshadigital.com 
 *
 * 
 **/
require '../../dynamic/init.inc';
$PAGE_ID = preg_replace('/[^a-zA-Z0-9._-]/', '', isset($_GET['q']) ? $_GET['q'] : '');
$TS      = time();
$files  = array(
    '../js/lib/jquery-1.7.1.js',
    '../js/lib/jquery.noty.js',
    '../js/lib/jquery.scrollTo-1.4.2-min.js',
    '../js/lib/json2.js',
    '../js/lib/date.format.js',
    '../js/lib/jquery.confirm.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-modal.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-alert.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-collapse.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-dropdown.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-typeahead.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-tooltip.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-popover.js',
    '../js/lib/bootstrap-2.0.3/bootstrap-scrollspy.js',
    '../js/lib/bootstrap-datepicker.js',
    '../js/lib/jquery.tablesorter.min.js',
    '../js/lib/jquery.tablesorter.pager.js',
    '../js/lib/jquery.cookie.19032012.js',
    '../js/helpers.js?t='.$TS,
    '../js/js.php?q='.$PAGE_ID,
    '../js/jquerylib.js?t='.$TS,
	'../js/Blob.js',
	'../js/FileSaver.js',
	'../js/opensave.js'
	
);
$wysiwyg  = '../js/lib/tinymce/jscripts/tiny_mce/tiny_mce_gzip.php?js=1&amp;plugins=autolink,lists,pagebreak,style,';
$wysiwyg .= 'layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,';
$wysiwyg .= 'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,';
$wysiwyg .= 'xhtmlxtras,template,advlist&amp;themes=advanced&amp;languages=en&amp;diskcache=true&amp;src=false';
switch ($PAGE_ID) {
case '2.16.1':
    // Below are the only ones needed for the Calendar page
    $files[1] = '../js/lib/jquery-ui-1.8.18.custom.js';
    $files[2] = '../js/lib/jquery.weekcalendar.js';
    $files[3] = '../js/lib/date.js';
    break;
case '2.4.9':
    // Below are the only ones needed for the View Roster Rule page
case '2.2.1':
    // Below are the only ones needed for the Scan Schedule page
    $files[1] = '../js/lib/jquery-ui-1.8.18.custom.js';
    $files[2] = '../js/lib/jquery.weekcalendar.js';
    $files[3] = '../js/lib/date.js';
    $files[4] = '../js/lib/jquery.noty.js';
    break;
case '2.8.4.2':
    // Below are the only ones needed for the Scan Schedule page
    $files[] = '../js/lib/pdfobject.js';
    $files[] = '../js/jqueryvalidation-2.8.4.2.js';
    break;
case '3.12.3':
case '3.12.4':
case '3.12.5':
case '3.12.6':
    // charting libraries
    $files[] = '../js/lib/jquery.flot.js';
    break;
case '2.1.5':
    // colorpicker
    $files[] = '../js/lib/bootstrap-colorpicker.js';
    break;
case '2.9.5':
    // querybuilder
    $files[] = '../js/querybuilder.js';
    break;
case '2.8.2.v2':
    $files[] = '../js/lib/tiny_mce/jquery.tinymce.js';
    $files[] = '../js/lib/tiny_mce/tiny_mce.js';
    $files[] = $wysiwyg;
    break;
case '2.8.4.v2':
    $files[] = '../js/lib/tiny_mce/jquery.tinymce.js';
    $files[] = '../js/lib/tiny_mce/tiny_mce.js';
    $files[] = $wysiwyg;
    break;
case '2.6.5':
    
	$files[] = '../js/opensave.js';
    break;
default:
    break;
}
header('Content-type: application/javascript');
?>
$LAB.script(<?php echo join(',', $files); ?>);
