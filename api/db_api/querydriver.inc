<?php
/**
 * Create the database layer.
 *
 * PHP version 5
 *
 * @category  DB_Layer
 * @package   
 * @author  sumit <sumit.jain.knit@gmail.com>
 * @copyright 
 * @license  
 * @link      
*/

/**
 * class create the QueryDriver.
 *
 * @category QueryDriver
 * @package  LIS
 * @author   Sumit Kumar Jain <sumit.jain@mokshadigital.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     http://192.168.10.30/playground/dummy.php
*/

class QueryDriver
{
    var $database;

    /**
    * __construct class constructor
    *
    * @param string $database database that should be used in this class
    *
    * @return none
    */

    public function __construct($database='mysql')
    {
        $this->database = $database;
    }
    /**
    * specialquote quote the column for use in queries
    *
    * @param string $name can be table or column which needs to be quoted
    *
    * @return return the select query
    */

    function specialquote($name)
    {
        if ($this->database === 'mysql') {
            return '`'.$name.'`';
        } else if ($this->database === 'postgres') {
            return '"'.$name.'"';
        } else {
            return $name;
        }
    }

    /**
    * getSelectQuery create the Query for select statement.
    *
    * @param string $table it contain the table name of db.
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the select query
    */

    function getSelectQuery($table,$data)
    {
        $query = '';
        if ($table != '') {
            if (!(is_null($data['columns'])) && ($data['columns'] != '')) {
                $length = count($data['columns']);
                $check  = 0;
                for ( $i = 0; $i < $length ; $i++) {
                    if ((is_null($data['columns'][$i]))
                        || ($data['columns'][$i] == '')
                    ) {
                        $check = 0;
                    } else {
                        $check = 1;
                        break;
                    }
                }
                if (($length == 0) || ($check == 0) ) {
                    $qtable = $this->specialquote($table);
                    $query = 'SELECT * FROM '.$qtable;
                } else {
                    $query = 'SELECT ' ;
                    for ( $i = 0; $i < $length; $i++) {
                        if (!(is_null($data['columns'][$i]))
                            && ($data['columns'][$i] != '')
                        ) {
                            $qcol   = $this->specialquote($data['columns'][$i]);
                            $query .= $qcol.' , ' ;
                        }
                    }
                    $query  = substr($query, 0, -2); // subtracting the last comma.
                    $qtable = $this->specialquote($table);
                    $query .= ' FROM '.$qtable ;
                }
                $check1  = 0;
                if (isset($data['where'])) {
                    $length1 = count($data['where']);
                    for ( $i = 0; $i < $length1; $i++) {
                        if ((is_null($data['where'][$i]))
                            || ($data['where'][$i] == '')
                        ) {
                            $check1 = 0;
                        } else {
                            $check1 = 1;
                            break;
                        }
                    }
                    if (($data['where'] == '') || (is_null($data['where']))
                        || ($check1 == 0)
                    ) {
                    } else {
                        $query .= ' WHERE ';
                        for ( $i = 0; $i < $length1; $i++) {
                            if (!(is_null($data['where'][$i]))
                                && ($data['where'][$i] != '')
                            ) {
                                $query .= $this->specialquote($data['where'][$i]).'  = :'.$data['where'][$i].' AND ' ;
                            }
                        }
                        $query = substr($query, 0, -4);
                    }

                    /*
                        COMMENTS
                        $query .= ' WHERE ';
                        if (($data['condition'] == '') || (is_null($data['condition'])) || ($data['condition'] == 'AND')) {
                            for ($i = 0; $i<$length1; $i++) {
                                if (!(is_null($data['where'][$i]))
                                    && ($data['where'][$i] != '')
                                ) {
                                    $query .= $this->specialquote($data['where'][$i]) . '  = :' .
                                                $data['where'][$i] . ' AND ' ;
                                }
                            }
                            $query = substr($query, 0, -4);
                        } else if ($data['condition']=='OR') {
                            for ( $i = 0; $i<$length1; $i++) {
                                if (!(is_null($data['where'][$i]))
                                    && ($data['where'][$i] != '')
                                ) {
                                    $query .= $this->specialquote($data['where'][$i]) . '  = :' .
                                                $data['where'][$i] . ' OR ' ;
                                }
                            }
                            $query = substr($query, 0, -3);
                        }
                    */

                }
            }
        }
        return $query;
    }
    /**
    * getSelectForOracle create select query for oracle..
    *
    * @param string $query it contain the query without limit and offset..
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the select query
    */
    function getSelectForOracle($query,$data)
    {
        if (((is_null($data['limit']))) || ($data['limit'] == '')) {
        } else {
            if (((is_null($data['offset'])))
                || ($data['offset'] == '')
            ) {
            } else {
                      $query .= ' ROWNUM >= '.$data['offset'].' ROWNUM < '.$data['limit'];
            }
        }
        return $query;
    }
    /**
    * getSelectForSqlite create select query for sqlite..
    *
    * @param string $query it contain the query without limit and offset..
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the select query
    */
    function getSelectForSqlite($query, $data)
    {
        if (((is_null($data['limit']))) || ($data['limit'] == '')) {
        } else {
            if (((is_null($data['offset']))) || ($data['offset'] == '')) {
                         $query .= ' LIMIT '.$data['limit'];
            } else {
                      $query .= ' LIMIT '.$data['limit'].' OFFSET '.$data['offset'];
            }
        }
        return $query;
    }
    /**
    * getSelectForMysql create select query for Mysql..
    *
    * @param string $query it contain the query without limit and offset..
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the select query
    */
    function getSelectForMysql($query,$data)
    {
        if ((isset($data['orderby'])) && (isset($data['order']))) {
            if ((is_null($data['orderby'])) || ($data['orderby'] == '')) {
            } else {
                $query .= ' ORDER BY '.$data['orderby'];
                if ((is_null($data['order'])) || ($data['order'] == '')) {
                } else {
                    $query .= ' '.$data['order'];
                }
            }
        }

        return $query;
    }
    /**
    * getSelectForPostgres create select query for Postgres..
    *
    * @param string $query it contain the query without limit and offset..
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the select query
    */
    function getSelectForPostgres($query,$data)
    {
        if (((is_null($data['limit']))) || ($data['limit'] == '')) {
        } else {
            if (((is_null($data['offset']))) || ($data['offset'] == '')) {
                         $query .= ' LIMIT '.$data['limit'];
            } else {
                      $query .= ' LIMIT '.$data['limit'].' OFFSET '.$data['offset'];
            }
        }
        return $query;
    }
    /**
    * getUpdateQuery create the Query for Update statement.
    *
    * @param string $table it contain the table name of db.
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the update query
    */
    function getUpdateQuery($table,$data)
    {
        $query = '';
        if ($table != '') {
            if (!(is_null($data['columns'])) && ($data['columns'] != '')) {
                $length = count($data['columns']);
                $check  = 0;
                for ( $i = 0; $i < $length; $i++) {
                    if ((is_null($data['columns'][$i]))
                        || ($data['columns'][$i] == '')
                    ) {
                        $check = 0;
                    } else {
                        $check = 1;
                        break;
                    }
                }
                if ( ($length != 0) && $check == 1) {
                    $qtable = $this->specialquote($table);
                    $query = 'UPDATE '.$qtable.' SET ';
                    for ( $i = 0; $i < $length; $i++) {
                        if (!(is_null($data['columns'][$i]))
                            && ($data['columns'][$i] != '')
                        ) {
                            $query .= $this->specialquote($data['columns'][$i]).'  = :'.$data['columns'][$i].' , ' ;
                        }
                    }
                    $query = substr($query, 0, -2);
                    if ( $data['where'] != '') {
                        $length1 = count($data['where']);
                        $check1  = 0;
                        for ( $i = 0; $i < $length1 ; $i++) {
                            if ((is_null($data['where'][$i]))
                                || ($data['where'][$i] == '')
                            ) {
                                $check1 = 0;
                            } else {
                                $check1 = 1;
                                break;
                            }
                        }
                        if ( $check1 == 1) {
                            $query .= ' WHERE ';
                            for ( $i = 0; $i < $length1; $i++) {
                                if (!(is_null($data['where'][$i]))
                                    && ($data['where'][$i] != '')
                                ) {
                                    $query .= $this->specialquote($data['where'][$i]).'  = :'.$data['where'][$i].' AND ' ;
                                }
                            }// subtracting last AND from the query.
                            $query = substr($query, 0, -4);
                        }
                    }

                }
            }
        }
         return $query;

    }
    /**
    * getDeleteQuery create the Query for delete statement.
    *
    * @param string $table it contain the table name of db.
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the delete query
    */
    function getDeleteQuery($table,$data)
    {
        $query = '';
        if ($table != '') {
            $qtable = $this->specialquote($table);
            $query = 'DELETE FROM '.$qtable ;
            if (!(is_null($data['where'])) && ($data['where'] != '')) {
                $length1 = count($data['where']);
                if ($length1 == 0) {
                     $query .= ' , ;';
                } else {
                    $check1 = 0;
                    for ( $i = 0; $i < $length1 ; $i++) {
                        if ((is_null($data['where'][$i]))
                            || ($data['where'][$i] == '')
                        ) {
                            $check1 = 0;
                        } else {
                            $check1 = 1;
                            break;
                        }
                    }
                    if ($check1 == 1) {
                        $query .= ' WHERE ';
                        for ( $i = 0; $i < $length1; $i++) {
                            if (!(is_null($data['where'][$i]))
                                && ($data['where'][$i] != '')
                            ) {
                                $query .= $this->specialquote($data['where'][$i]).'  = :'.$data['where'][$i].' AND ' ;
                            }
                        } // subtracting last AND from the query.
                        $query = substr($query, 0, -4);
                    }
                }
            }

        }
         return $query;

    }
    /**
    * getInsertQuery create the Query for insert statement.
    *
    * @param string $table it contain the table name of db.
    * @param array  $data  it contain the column name of the table.
    *
    * @return return the insert query
    */
    function getInsertQuery($table,$data)
    {
        $query = '';
        if ($table != '') {
            if (!(is_null($data['columns'])) && ($data['columns'] != '')) {
                $length = count($data['columns']);
                $check  = 0;
                for ( $i = 0; $i < $length; $i++) {
                    if ((is_null($data['columns'][$i]))
                        || ($data['columns'][$i] == '')
                    ) {
                        $check = 0;
                    } else {
                          $check = 1;
                          break;
                    }
                }
                if (($length != 0) && ($check == 1)) {
                    $qtable = $this->specialquote($table);
                    $query .= 'INSERT INTO '.$qtable.' ( ' ;
                    for ( $i = 0; $i < $length; $i++) {
                        if (!(is_null($data['columns'][$i]))
                            && ($data['columns'][$i] != '')
                        ) {
                            $query .= $this->specialquote($data['columns'][$i]).' , ' ;
                        }
                    }// subtracting last ',' from the query.
                    $query  = substr($query, 0, -2);
                    $query .= ' ) values ( ';
                    for ( $i = 0; $i < $length; $i++) {
                        if (!(is_null($data['columns'][$i]))
                            && ($data['columns'][$i] != '')
                        ) {
                            $query .= ' :'.$data['columns'][$i].' , ' ;
                        }
                    } // subtracting last ',' from the query.
                    $query  = substr($query, 0, -2);
                    $query .= ')';
                }
            }
        }
        return $query;

    }

}

/*
    COMMENTS
    $dbname = array('sqlite','oracle','mysql','postgres');
    $table  = 'PatientInsurance';
    $data   = array(array('columns' => array('ContactPerson', 'Address'),
                      'where' => array('ContactPerson', 'Address'),
                      'limit' => 10 ,'offset'=>0),
                array('columns' => array('', 'Address'),
                      'where' => array('ContactPerson', 'Address'),
                      'limit' =>10 , 'offset'=>''),
                array('columns' => array('', ''),
                      'where' => array('ContactPerson', 'Address'),
                      'limit' => '' , 'offset' => 10),
                array('columns' => array('ContactPerson', ''),
                      'where' => array('ContactPerson', 'Address'),
                      'limit' =>null , 'offset'=>10),
                array('columns' => array('ContactPerson', 'Address'),
                      'where' => array('', 'Address'),
                      'limit' =>'' , 'offset'=>''),
                array('columns' => array('ContactPerson', 'Address'),
                      'where' => array('', ),
                      'limit' =>null , 'offset'=>null),
                array('columns' => array('', ''),
                      'where' => array('', ''),
                      'limit' =>10 , 'offset'=>10),
                array('columns' => array(null, 'Address'),
                      'where' => array('ContactPerson', 'Address'),
                      'limit' =>10 , 'offset'=>10),
                array('columns' => array(null, null),
                      'where' => array('ContactPerson', 'Address'),
                      'limit' =>'' , 'offset'=>15),
                array('columns' => array('ContactPerson', 'Address'),
                      'where' => array(null, null),
                      'limit' =>10 , 'offset'=>1)
          );
    $comb   = count($data);
    $bar = new QueryDriver;
    foreach ($dbname as $dbs) {
        for ($i =0 ; $i<$comb; $i++) {
            $bar->database = $dbs;
            $queryselect1 = $bar->getSelectQuery($table, $data[$i]);
            switch ($dbs) {
            case 'oracle':
                print "[oracle] ";
                $queryselect1 = $bar->getSelectForOracle($queryselect1, $data[$i]);
                print $queryselect1;
                echo "\n";
                break;
            case 'sqlite':
                print "[sqlite] ";
                $queryselect1 = $bar->getSelectForSqlite($queryselect1, $data[$i]);
                print $queryselect1;
                echo "\n";
                break;
            case 'mysql':
                print "[mysql] ";
                $queryselect1 = $bar->getSelectForMysql($queryselect1, $data[$i]);
                print $queryselect1;
                echo "\n";
                break;
            case 'postgres':
                print "[postgres] ";
                $queryselect1 = $bar->getSelectForPostgres($queryselect1, $data[$i]);
                print $queryselect1;
                echo "\n";
                break;
            }
        }
    }
    for ($i =0 ; $i<$comb; $i++) {
        $queryupdate1 = $bar->getUpdateQuery($table, $data[$i]);
        print $queryupdate1;
        echo "\n";
    }
    for ($i =0 ; $i<$comb; $i++) {
        $querydelete1 = $bar->getDeleteQuery($table, $data[$i]);
        print $querydelete1;
        echo "\n";
    }
    for ($i =0 ; $i<$comb; $i++) {
        $queryinsert1 = $bar->getInsertQuery($table, $data[$i]);
        print $queryinsert1;
        echo "\n";
    }

*/

// DONT THINK OF PUTTING ENDING TAG BELOW
