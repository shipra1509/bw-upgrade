<?php
/**
 * Create middleware for db.
 *
 * PHP version 5
 *
 * @category  Middleware
 * @package   LIS
 * @author    Sumit Kumar Jain <sumit.jain@mokshadigital.com>
 * @copyright 2011-2012 The moksha Group
 * @license   http://www.mokshadigital.com/eula.html MOKSHA 1.0
 * @link      http://www.mokshadigital.com
*/

define('MIDDLEWARE_MYSQL', 'mysql');
define('MIDDLEWARE_ORACLE', 'oracle');
define('MIDDLEWARE_POSTGRES', 'postgres');
define('MIDDLEWARE_POSTGRESQL', 'postgres');
define('MIDDLEWARE_SQLITE', 'sqlite');

define('MIDDLEWARE_MYSQL_PDO', 'mysql');
define('MIDDLEWARE_ORACLE_PDO', 'oci');
define('MIDDLEWARE_POSTGRES_PDO', 'pgsql');
define('MIDDLEWARE_POSTGRESQL_PDO', 'pgsql');
define('MIDDLEWARE_SQLITE_PDO', 'sqlite');
define('MIDDLEWARE_SQLITE3_PDO', 'sqlite');
define('MIDDLEWARE_SQLITE2_PDO', 'sqlite2');

/**
 * class create the connection and perform operation.
 *
 * @category Execution_Of_Query
 * @package  
 * @author  sumit <sumit.jain.knit@gmail.com>
 * @license  
 * @link     
*/
class Middleware
{
     var $objectdb;
     var $ob;
     var $columnsarray;
     var $obj;
     var $out;
     var $config;
     var $username;
     var $password;
     var $database;
     var $inTransactionMode;
     private static $_middleware;

    /**
    * getInstance singleton instance of the class middleware
    *
    * @param string $pathdbconfig  path of dbconfig
    * @param string $pathqueryjson path of query.json
    *
    * @return return the db connection
    */
    public static function getInstance($pathdbconfig, $pathqueryjson)
    {
        if (!self::$_middleware) {
            self::$_middleware = new Middleware($pathdbconfig, $pathqueryjson, MIDDLEWARE_POSTGRES);
        }
        return self::$_middleware;
    }

    /**
    * __construct create the connection with database.
    *
    * @param string $pathdbconfig  path of dbconfig
    * @param string $pathqueryjson path of query.json
    * @param string $db            Name of db 
    *
    * @return return the db connection    
    */
    public function __construct($pathdbconfig , $pathqueryjson ,$db=MIDDLEWARE_MYSQL)
    {
        if (is_null($pathdbconfig)) {
            throw new Exception(
                'Application Configuration Path is not passed.'
            );
        }
        if (is_null($pathqueryjson)) {
            throw new Exception(
                'Application Query Configuration Path is not passed.'
            );
        }
        if (is_null($db)) {
            throw new Exception('Application Database name is not passed.');
        }
        if (!file_exists($pathdbconfig)) {
            throw new Exception(
                'Application Configuration Path does not exist in filesystem : '.$pathdbconfig
            );
        }
        if (!file_exists($pathdbconfig)) {
            throw new Exception(
                'Application Query Configuration Path does not exist in filesystem : '.$pathdbconfig
            );
        }
        $configfile     = file_get_contents($pathdbconfig);
        $configdata     = json_decode($configfile, true);
        $this->config   = $configdata['config'];
        $cfgparts       = preg_split('/:/', $this->config);
        $this->username = $configdata['username'];
        $this->password = $configdata['password'];
        $this->database = $cfgparts[0];
        switch($cfgparts[0]) {
        case MIDDLEWARE_MYSQL_PDO;
            $this->database = MIDDLEWARE_MYSQL;
            break;
        case MIDDLEWARE_ORACLE_PDO;
            $this->database = MIDDLEWARE_ORACLE;
            break;
        case MIDDLEWARE_POSTGRES_PDO;
            $this->database = MIDDLEWARE_POSTGRES;
            break;
        case MIDDLEWARE_SQLITE_PDO;
            $this->database = MIDDLEWARE_SQLITE;
            break;
        default:
            throw(new Exception('Configuration Error. Invalid Database Type'));
            break;
        }
        $this->objectdb     = new DB(
            $this->config, $this->username, $this->password
        );
        $this->ob           = new GenerateQuery($pathqueryjson);
        $this->columnsarray = $this->ob->getcolumnsforquery();
        $this->obj          = new QueryDriver($this->database);
    }
    /**
    * performDB perform the operation on DB..
    *
    * @param string $operation it contain the operation.
    * @param string $value     it contain the value of column.
    * @param string $limit     it contain the limit value.
    * @param string $offset    it contain the offset value.
    *        
    * @return return status, message and number of deleted rows.
    */
    function performDB($operation,$value,$limit=1000,$offset=0)
    {
        $querytype = $this->columnsarray[$operation]['querytype'];
        switch($querytype) {
        case 'insert' : {
            $queryinsert       = $this->obj->getInsertQuery(
                $this->columnsarray[$operation]['entity'],
                $this->columnsarray[$operation]
            );
            $insertresult      = $this->objectdb
                ->runInsertQuery($queryinsert, $value);
            $this->out         = $insertresult;
            break;
        }
        case 'delete' : {
            $querydelete       = $this->obj->getDeleteQuery(
                $this->columnsarray[$operation]['entity'],
                $this->columnsarray[$operation]
            );
            $deleteresult      = $this->objectdb
                ->runDeleteQuery($querydelete, $value);
            $this->out         = $deleteresult;
            break;
        }
        case 'update' : {
            $queryupdate       = $this->obj->getUpdateQuery(
                $this->columnsarray[$operation]['entity'],
                $this->columnsarray[$operation]
            );
            $updateresult      = $this->objectdb
                ->runUpdateQuery($queryupdate, $value);
            $this->out         = $updateresult;
            break;
        }
        case 'select' : {
            $queryselect       = $this->obj->getSelectQuery(
                $this->columnsarray[$operation]['entity'],
                $this->columnsarray[$operation]
            );
            switch ($this->database) {
            case MIDDLEWARE_MYSQL : {
                if ((!isset($limit)) && (!isset($offset))) {
                    $queryselect = $this->obj->getSelectForMysql(
                        $queryselect, $this->columnsarray[$operation]
                    );
                } else if ((isset($limit)) && (isset($offset))
                    || isset($this->columnsarray[$operation]['orderby'])
                ) {
                    $queryselect = $this->obj->getSelectForMysql(
                        $queryselect, $this->columnsarray[$operation]
                    );
                    $queryselect .= ' LIMIT '.$limit.' OFFSET '.$offset;
                }
                $selectresult      = $this->objectdb
                    ->runSelectQuery($queryselect, $value);
                $this->out         = $selectresult;
                break;
            }
            case MIDDLEWARE_ORACLE : {
                $queryselect = $this->obj->getSelectForOracle(
                    $queryselect, $this->columnsarray[$operation]
                );
                $selectresult      = $this->objectdb
                    ->runSelectQuery($queryselect, $value);
                $this->out         = $selectresult;
                break;
            }
            case MIDDLEWARE_POSTGRES : {
                $queryselect = $this->obj->getSelectForPostgres(
                    $queryselect, $this->columnsarray[$operation]
                );
                $selectresult      = $this->objectdb
                    ->runSelectQuery($queryselect, $value);
                $this->out         = $selectresult;
                break;
            }
            case MIDDLEWARE_SQLITE : {
                $queryselect = $this->obj->getSelectForSqlite(
                    $queryselect, $this->columnsarray[$operation]
                );
                $selectresult      = $this->objectdb
                    ->runSelectQuery($queryselect, $value);
                $this->out         = $selectresult;
                break;
            }
            default : {
                $selectresult      = $this->objectdb
                    ->runSelectQuery($queryselect, $value);
                $this->out         = $selectresult;
            }
            }
            break;
        }
        case 'specific' : {
            $query = $this->columnsarray[$operation]['query'];
            if (preg_match('/select /i', $query)) {
                $query .= ' LIMIT '.$limit.' OFFSET '.$offset;
            }
            $specificresult      = $this->objectdb
                ->runSpecificQuery($query, $value);
            $this->out         = $specificresult;
            break;
        }
        }
        return $this->out;
    }
    /**
    * performSpecificQuery perform the operation on DB..
    *    
    * @param string $query it contain the query.
    *        
    * @return return status, message and number of deleted rows.
    */
    function performSpecificQuery($query)
    {
            $specificresult      = $this->objectdb
                ->runSpecificQuery($query);
            $this->out         = $specificresult;
        return $this->out;
    }
}

// DONT THINK OF PUTTING ENDING TAG BELOW
