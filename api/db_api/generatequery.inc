<?php
/**
 * Create the unified parser.
 *
 * PHP version 5
 *
 * @category  
 * @package   
 * @author    Sumit <sumit.jain.knit@gmail.com>
 * @copyright 
 * @license   
 * @link      
*/

/**
 * class create the GenerateQuery.
 *
 * @category 
 * @package  
 * @author   Sumit <sumit.jain.knit@gmail.com>
 * @license  
 * @link     
*/
class GenerateQuery
{
    var $queryjson;
    var $query_configuration;
    var $query_config_array;
    /**
    * construct decode the query.json.
    *
    * @param string $queryjson it contain the file name of unified json.  
    *    
    * @return it return nothing 
    */
    public function __construct($queryjson)
    {
        $this->queryjson           = $queryjson;
        $this->query_configuration = file_get_contents($this->queryjson);
        $this->query_config_array  = json_decode(
            $this->query_configuration, true
        );
		//print_r($this->query_config_array);
    }
    /**
    * get columns from query.json.
    *       
    * @return it return all columns
    */
    function getcolumnsforquery()
    {
        foreach (array_keys($this->query_config_array) as $key) {
            $columns[$key]['querytype'] = $this->
                                          query_config_array[$key]['querytype'];
            $columns[$key]['entity']    = $this->
                                          query_config_array[$key]['entity'];
            if (isset($this->query_config_array[$key]['where'][0])) {
                if (!preg_match('/^@/', $this->query_config_array[$key]['where'][0])
                ) {
                    $columns[$key]['where']
                        = $this->query_config_array[$key]['where'];
                } else {
                    $unified                = substr(
                        $this->query_config_array[$key]['where'][0], 1
                    );
                    $obwhere                = new UnifiedParser($unified);
                    $columns[$key]['where'] = $obwhere->getAllElements();
                }
            }
            if (isset($this->query_config_array[$key]['columns'][0])) {
                if (!preg_match('/^@/', $this->query_config_array[$key]['columns'][0])) {
                    $columns[$key]['columns'] = $this->
                                            query_config_array[$key]['columns'];
                } else {
                    $unified                  = substr(
                        $this->query_config_array[$key]['columns'][0], 1
                    );
                    $object                   = new UnifiedParser($unified);
                    $columns[$key]['columns'] = $object->getAllElements();
                }
            } else {
                $columns[$key]['columns'] = array();
            }
            if (isset($this->query_config_array[$key]['limit'])) {
                $columns[$key]['limit']  = $this->query_config_array[$key]['limit'];
            }
            if (isset($this->query_config_array[$key]['offset'])) {
                $columns[$key]['offset'] = $this->query_config_array[$key]['offset'];
            }
            if (isset($this->query_config_array[$key]['orderby'])) {
                $columns[$key]['orderby']
                    = $this->query_config_array[$key]['orderby'];
            }
            if (isset($this->query_config_array[$key]['order'])) {
                $columns[$key]['order'] = $this->query_config_array[$key]['order'];
            }
            if (isset($this->query_config_array[$key]['query'])) {
                $columns[$key]['query'] = $this->query_config_array[$key]['query'];
            }
            if (isset($this->query_config_array[$key]['condition'])) {
                $columns[$key]['condition'] = $this->query_config_array[$key]['condition'];
            }
        }
        return $columns;
    }
    /**
    * get table Name from query.json.
    *       
    * @return it return tablename
    */
    function gettablename()
    {
        foreach (array_keys($this->query_config_array) as $key) {
            $table[] = $this->query_config_array[$key]['entity'];
        }
        return $table;
    }
}

// DONT THINK OF PUTTING ENDING TAG BELOW
