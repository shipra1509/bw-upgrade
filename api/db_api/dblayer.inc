<?php

/**
 * Create the database layer.
 *
 * PHP version 5
 *
 * @category  DB_Layer
 * @package   
 * @author  sumit <sumit.jain.knit@gmail.com>
 * @copyright 
 * @license   
 * @link     
*/


/**
 * class execute the query.
 *
 * @category Execution_Of_Query
 * @package  LIS
 * @author   Sumit Kumar Jain <sumit.jain@mokshadigital.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     www.mokshadigital.com
*/
class DB
{
    var $config;
    var $username;
    var $password;
    var $rval;
    var $pdo;
    var $pdocfg;

    /**
    * init create the connection with database.
    *
    * @param string $config   it contain the host name of system and dbname.
    * @param string $username it contain the user name of db.
    * @param string $password it contain the password of db.
    *
    * @return return the db connection
    */

    public function __construct($config, $username, $password)
    {
        $this->config   = $config;
        $this->username = $username;
        $this->password = $password;
        $this->pdocfg   = array(
            // FIXME: Change this for other types of databases
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            // Log all badly written queries
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
            // don't use persistent connections
            //PDO::ATTR_PERSISTENT => false
            // use buffered query for mysql
            PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
        );
        try {
            $this->pdo = new PDO(
                //$this->config, $this->username, $this->password, $this->pdocfg
                $this->config, $this->username, $this->password, $this->pdocfg
            );
        } catch(Exception $e)
    {
              $this->rval = array(
                                'status'  => 'failure',
                                'message' => $e->getMessage()
                            );

        }
    }
    /**
    * select retrieve the data from database.
    *
    * @param string $query  it contain the query.
    * @param string $params it contain the value of column.
    *
    * @return return the status, message and data of select query.
    */
    public function runSelectQuery($query,$params)
    {
        if (($this->rval['status'] === 'failure') === false) {
            try {
                $sth         = $this->pdo->prepare($query);
                if ($sth->execute($params) === true) {
                    $result      = $sth->fetchAll(PDO::FETCH_ASSOC);
                    $selectedrow = count($result);
                    $this->rval  = array('status' => 'success', 'message' => '','data' => $result);
                } else {
                    logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                    $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
                }
                $sth->closeCursor();
            } catch (PDOException $e) {
                logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
            }
        }
        return $this->rval;

    }
    /**
    * update update the data from database.
    *
    * @param string $query  it contain the query.
    * @param string $params it contain the value of column.
    *
    * @return return status, message and number of updated rows.
    */
    function runUpdateQuery($query,$params)
    {
        if (($this->rval['status'] === 'failure') === false) {
            try {
                $sth        = $this->pdo->prepare($query);
                if ($sth->execute($params) === true) {
                    $updatedrow = $sth->rowCount();
                    // fetch the num rows affected
                    if ($updatedrow <= 0) {
                        $row         = $this->pdo->query('SELECT ROW_COUNT()')->fetch();
                        $updatedrow  = $row[0];
                    }
                    $this->rval = array('status' => 'success', 'message' => '','data' => $updatedrow);
                } else {
                    logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                    $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
                }
                unset($sth);
            } catch (PDOException $e) {
                logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
            }
        }
        return $this->rval;
    }
    /**
    * delete delete row from database.
    *
    * @param string $query  it contain the query.
    * @param string $params it contain the value of column.
    *
    * @return return status, message and number of deleted rows.
    */
    function runDeleteQuery($query,$params)
    {
        if (($this->rval['status'] === 'failure') === false) {
            try {
                $sth        = $this->pdo->prepare($query);
                if ($sth->execute($params) === true) {
                    $deletedrow = $sth->rowCount();
                    // fetch the num rows affected
                    if ($deletedrow <= 0) {
                        $row         = $this->pdo->query('SELECT ROW_COUNT()')->fetch();
                        $deletedrow  = $row[0];
                    }
                    $this->rval = array('status' => 'success', 'message' => '','data' => $deletedrow);
                } else {
                    logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                    $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
                }
                unset($sth);
            } catch (PDOException $e) {
                logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
            }
        }
        return $this->rval;
    }
    /**
    * insert insert row into database.
    *
    * @param string $query  it contain the query.
    * @param string $params it contain the value of column.
    *
    * @return return status, message and number of inserted row.
    */
    function runInsertQuery($query,$params)
    {
        if (($this->rval['status'] === 'failure') === false) {
            try {
                $insertid    = -1;
                $insertedrow = 0;
                $sth         = $this->pdo->prepare($query);
                if ($sth->execute($params) === true) {
                    $insertid    = $this->pdo->lastinsertid();
                    $insertedrow = $sth->rowCount();

                    /*
                        FETCH the insert id
                        $row         = $this->pdo->query("SELECT LAST_INSERT_ID()")->fetch();
                        $insertid    = $row[0];
                    */

                    // fetch the num rows affected
                    $row         = $this->pdo->query('SELECT ROW_COUNT()')->fetch();
                    $insertedrow = $row[0];

                    logmymessage(array('inserted with id = ', $insertid, 'row count = ', $insertedrow));
                    $this->rval  = array('status' => 'success', 'message' => '', 'data' => $insertedrow, 'insertid' => $insertid);
                } else {
                    logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                    $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
                }
                unset($sth);
            } catch (PDOException $e) {
                logmymessage(array('query' => $query, 'params' => $params, 'pdoerrors' => $sth->errorInfo()), MYLOG_NOTICE);
                $this->rval  = array('status' => 'failure', 'message' => $sth->errorInfo());
            }
        }
        return $this->rval;
    }
    /**
    * insert insert row into database.
    *
    * @param string $query it contain the query.
    * @param array  $args  column values
    *
    * @return return status, message and data.
    */
    function runSpecificQuery($query, $args=array())
    {
        if (($this->rval['status'] === 'failure') === false) {
            try {
                $sth         = $this->pdo->prepare($query);
                $ins         = $sth->execute($args);
                if (preg_match('/select /i', $query) === 1) {
                    $result  = $sth->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    $result  = array();
                }
                $this->rval  = array('status' => 'success', 'message' => '','data' => $result);
                $sth->closeCursor();
            } catch (PDOException $e) {
                 $this->rval = array( 'status' => 'failure', 'message' => $e->getMessage());
            }
        }
        return $this->rval;
    }
}

//DONT THINK OF PUTTING ENDING TAG BELOW
