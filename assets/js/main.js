$(document).ready(function(){
  /* ================ SMOOTH SCROLL ================ */
  $('a[href^="#"].down').on('click',function (e) {
    e.preventDefault();
    var target = this.hash,
    $target = $(target);
    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 300, 'swing', function () {
      window.location.hash = target;
    });
  });
  /* ================ SMOOTH SCROLL ends ================ */

  /* ================ STICKY NAVBAR ================ */
  $(function(){
        // Check the initial Poistion of the Sticky Header
        var stickyHeaderTop = $('#stickyheader').offset().top;

        $(window).scroll(function(){
          if( $(window).scrollTop() > stickyHeaderTop ) {
            $('#stickyheader').css({position: 'fixed', top: '0px'});
            $('#stickyalias').css('display', 'block');
          } else {
            $('#stickyheader').css({position: 'static', top: '0px'});
            $('#stickyalias').css('display', 'none');
          }
        });
      });
  /* ================ STICKY NAVBAR ends================ */
});


