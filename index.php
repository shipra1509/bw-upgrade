<!DOCTYPE html>
<html>
<head>
	<title>Bold Ware</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.0/build/pure-min.css"></head>
	<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.1/build/grids-responsive-old-ie-min.css">
	<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.1/build/grids-responsive-min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="assets/css/animated-slider.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- OWL CAROUSEL -->
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css'>
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css'>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js'></script>
	<script type="text/javascript" src="assets/js/jquery.cssslider.js"></script>
	<script src="assets/js/main.js"></script>
</head>
<body>
	<div id="fullpage">
		<section class="section one">
			<div class="banner1">
				<div class="container">
					<h3>We believe humans crave<br>joy and happiness</h3>
					<div class="circle">
						<div class="circle-orange"></div>
					</div> 
				</div>
			</div>
		</section>

		<section class="section second">
			<div class="banner2">
				<div class="container">
					<h3>Providing wonderful<br>experiences is one of the best<br>ways to achieve this!</h3>
					<div class="circle">
						<div class="circle-white"></div>
					</div>
				</div>
			</div>
		</section>

		<section class="section third">
			<div class="banner1">
				<div class="container">
					<h3>We do this digitally by<br>creating beautiful apps<br>for consumers and enterprises!</h3>
					<div class="circle">
						<div class="circle-orange"></div>
					</div> 
				</div>
			</div>
		</section>
	</div>

	<!-- <div class="banner1">
		<div class="container">
			<h3>We believe humans crave<br>joy and happiness</h3>
			<div class="circle">
				<div class="circle-orange"></div>
			</div> 
		</div>
	</div>
	
	<div class="banner1-curve-bg">
		<div class="banner1-curve">
		</div> 
	</div>
	 
	<div class="banner2">
		<div class="container">
			<h3>Providing wonderful<br>experiences is one of the best<br>ways to achieve this!</h3>
			<div class="circle">
				<div class="circle-white"></div>
			</div>
		</div>
	</div>
	
	<div class="banner2-curve-bg">
		<div class="banner2-curve">
		</div> 
	</div>
	
	<div class="banner1">
		<div class="container">
			<h3>We do this digitally by<br>creating beautiful apps<br>for consumers and enterprises!</h3>
			<div class="circle">
				<div class="circle-orange"></div>
			</div> 
		</div>
	</div>
	
	<div class="banner1-curve">
	</div> 
	
	  -->
<section>
		<div class="banner4">
			<div class="container">
				<img src="assets/img/bw.png" alt="">
				<h3>BOLDWARE</h3>
				<h2>Your design solution</h2>
				<div class="circle">
					<div class="circle-orange"></div>
				</div>
			</div>
		</div>
	</section>


	<nav id="stickyheader">
		<div class="navbar">
			<div class="container">
				<div class="pure-menu pure-menu-horizontal">
					<div class="pure-g">
						<div class="pure-u-1-5">
							<a href="#" class="pure-menu-heading pure-menu-link"><img src="assets/img/bw_logo.png" alt=""></a>
						</div>				
						<div class="pure-u-lg-4-5 nav-right">	
							<ul class="pure-menu-list">
							<!-- 	<li class="pure-menu-item"><a href="#" class="pure-menu-link">Why</a></li>
							<li class="pure-menu-item"><a href="#" class="pure-menu-link">How</a></li>
							<li class="pure-menu-item"><a href="#" class="pure-menu-link">What</a></li>
							<li class="pure-menu-item"><a href="#" class="pure-menu-link contact">Get in touch</a></li> -->
							<li class="pure-menu-item"><a href="#contact-us" class="pure-menu-link work down">Get in touch</a></li>
						</ul> 
					</div>
				</div>
			</div>
		</div>
	</div>
</nav>

<div id="stickyalias"></div>

<!-- ================ WHY ================= -->
<section>
	<div class="container">
		<div class="why">
			<!-- <div class="pure-g sub-tab">
				<h3>why</h3>
				<input id="tab1" type="radio" name="tabs" checked>
				<label for="tab1">Enterprise Efficiency</label>
				<input id="tab2" type="radio" name="tabs">
				<label for="tab2">Consumer Happiness</label>
				<section id="content1">
					<p>Bacon ipsum dolor sit amet</p>
				</section>
				<section id="content2">
					<p>Bacon ipsum dolor sit amet landja</p>
				</section>  
			</div> -->

			<div class="pure-g why-pure">
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
					<div class="why-box">
						<h5>Enterprise Efficiency</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum deleniti mollitia nemo. Beatae minus cumque, possimus natus quo reiciendis, earum nobis delectus distinctio? Perspiciatis laboriosam vel ad, error deleniti eius. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem ipsum quis enim magni est temporibus maxime repellat molestias perspiciatis. Id cumque unde aliquam illum doloribus facilis atque, voluptatum, sit voluptates.</p>
						<a href="#contact-us" class="view down">view case study</a>
					</div>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
					<div class="why-box">
					<div id="f1_container">
							<div id="f1_card">
								<div class="front face">
									<img src="assets/img/enterprise_efficiency.png" class="eff-img">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="pure-g why-pure">

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
					<div class="why-box">
						<div id="f1_container">
							<div id="f1_card">
								<div class="front face">
									<img src="assets/img/enterprise_efficiency.png" class="eff-img">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
					<div class="why-box">
						<h5>Consumer Happiness</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum deleniti mollitia nemo. Beatae minus cumque, possimus natus quo reiciendis, earum nobis delectus distinctio? Perspiciatis laboriosam vel ad, error deleniti eius. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem ipsum quis enim magni est temporibus maxime repellat molestias perspiciatis. Id cumque unde aliquam illum doloribus facilis atque, voluptatum, sit voluptates.</p>
						<a href="#contact-us" class="view down">view case study</a>
					</div>
				</div>


			</div>

		</div>
	</div>
</section>
<!-- ================ WHY ends ================= -->

<!-- ================ WHAT ================= -->
<!-- <section>
	<div class="container">
		<div class="why">
			<div class="pure-g sub-tab">
				<h3>why</h3>
				<input id="tab3" type="radio" name="tabs">
				<label for="tab3">Enterprise Efficiency</label>
				<input id="tab4" type="radio" name="tabs" checked>
				<label for="tab4">Consumer Happiness</label>
				<section id="content1">
					<p>Bacon ipsum dolor sit amet</p>
				</section>
				<section id="content2">
					<p>Bacon ipsum dolor sit amet landja</p>
				</section>
			</div>


			<div class="pure-g why-pure">

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
					<div class="why-box">
						<img src="assets/img/enterprise_efficiency.png" class="eff-img">
					</div>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
					<div class="why-box">
						<h5>Consumer Happiness</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum deleniti mollitia nemo. Beatae minus cumque, possimus natus quo reiciendis, earum nobis delectus distinctio? Perspiciatis laboriosam vel ad, error deleniti eius. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem ipsum quis enim magni est temporibus maxime repellat molestias perspiciatis. Id cumque unde aliquam illum doloribus facilis atque, voluptatum, sit voluptates.</p>
						<a href="">view case study</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- ================ WHAT ends ================= -->

<section>
	<div class="process">
		<div class="container">
			<!-- <div class="pure-g sub-tab">
				<h3>how</h3>
				<input id="tab5" type="radio" name="tabs" checked>
				<label for="tab5">The Process</label>
				<input id="tab6" type="radio" name="tabs">
				<label for="tab6">Constant Learning</label>
				<section id="content1">
					<p>Bacon ipsum dolor sit amet</p>
				</section>
				<section id="content2">
					<p>Bacon ipsum dolor sit amet landja</p>
				</section>  
			</div> -->
			<h4>The process</h4>

	<!-- 		<div class="pure-g why-pure">
		<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
			<div class="why-box">
				<div id="f1_container">
					<div id="f1_card">
						<div class="front face">
							<img src="assets/img/enterprise_efficiency.png" class="eff-img"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
			<div class="why-box">
				<h5>Conceptualization</h5>
				<p>While any concept looks good in thoughts, clarity comes only once it is written down! But even the simplest of ideas has various points which need to be covered. Our Analysis team takes detailed understanding of your business and operations and then creates Detailed Business Requirement Documents along with Flowcharts listing down each of the scenarios and exceptions which would be helpful in developing the entire software system.</p>
				<div class="process-team">
					<img src="assets/img/team1.jpg" alt="">
					<img src="assets/img/teamg.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
-->
<div class="pure-g why-pure">
	<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
		<div class="why-box">
			<div id="f1_container">
				<div id="f1_card">
					<div class="front face">
						<img src="assets/img/wireframe.png" class="eff-img wireframe-des"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
		<div class="why-box">
			<h5>Wireframe and Design</h5>
			<p>Joe Sparano says - "Good design is obvious; great design is transparent"<br>
				Probably the most important phase for a consumer app, the UX needs to be properly thought through and presented in a manner thats easily understood by the target audience. We aim to keep the latest design principles in mind while also understanding the needs of the consumer in a manner that the flow is seamless and smooth and the user does not need to think in using the app.</p>
				<div class="process-team">
					<a href="#contact-us" class="view down">view case study</a>
							<!-- <img src="assets/img/team2.jpg" alt="">
							<img src="assets/img/teamg2.jpg" alt="">
							<img src="assets/img/team1.jpg" alt=""> -->

						</div>
					</div>
				</div>
			</div>

			<div class="pure-g why-pure">
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
					<div class="why-box">
						<div id="f1_container">
							<div id="f1_card">
								<div class="front face">
									<img src="assets/img/development.png" class="eff-img"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
					<div class="why-box">
						<h5>Development</h5>
						<p>This phase being the longest - is where the code is written and structured. Using the latest development architecture, frameworks and methodologies in place as well as proper documentation, we write the code in a manner that whether you need to understand it or a new developer at a certain time, it is easy to understand, scalable as well as well structured.</p>
						<div class="process-team">
							<a href="#contact-us" class="view down">view case study</a>
							<!-- <img src="assets/img/team2.jpg" alt="">
							<img src="assets/img/teamg.jpg" alt=""> -->
						</div>
					</div>
				</div>
			</div>

			<div class="pure-g why-pure">
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
					<div class="why-box">
						<div id="f1_container">
							<div id="f1_card">
								<div class="front face">
									<img src="assets/img/testing.png" class="eff-img"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
					<div class="why-box">
						<h5>Testing and Stabalization</h5>
						<p>No code is 100% free. Those who say their's is are either lying or are newbies. However, not just from a client-company relationship, but more from an innate quality of our developers to do the best at whatever they do, we strive to keep the bugs and issues at the lowest. And on top of it our testers are probably the most critical people you might meet finding faults in everything. (There's a joke about our developers constantly trying to outdo the testers by not letting them find any bugs. ) Our testers are experienced in Selenium and other tools while the testing happens not just from a UAT perspective but to an extent where load testing is also done in a manner that the app is stable even at peak environments.</p>
						<div class="process-team">
							<a href="#contact-us" class="view down">view case study</a>
							<!-- <img src="assets/img/team3.jpg" alt="">
							<img src="assets/img/team1.jpg" alt=""> -->
						</div>
					</div>
				</div>
			</div>

			<!-- <div class="pure-g why-pure">
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 pic-block half-block">
					<div class="why-box">
						<div id="f1_container">
							<div id="f1_card">
								<div class="front face">
									<img src="assets/img/enterprise_efficiency.png" class="eff-img"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-2 half-block">
					<div class="why-box">
						<h5>Deployment</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum deleniti mollitia nemo. Beatae minus cumque, possimus natus quo reiciendis, earum nobis delectus distinctio? Perspiciatis laboriosam vel ad, error deleniti eius. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem ipsum quis enim magni est temporibus maxime repellat molestias perspiciatis. Id cumque.</p>
						<div class="process-team">
							<img src="assets/img/team1.jpg" alt="">
							<img src="assets/img/teamg.jpg" alt="">
						</div>
					</div>
				</div>
			</div> -->

		</div>
	</div>
</section>


<!-- ================ TEAM ================= -->
<!-- <section>
	<div class="team">
		<div class="container">
			<h4>Our Team</h4>

			<div class="pure-g">
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/team1.jpg" alt="">
						<div class="team-info">
							<h5>saurabh pant</h5>
							<hr>
							<h6>Android Developer</h6>
						</div>
					</div></a>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/teamg2.jpg" alt="">
						<div class="team-info">
							<h5>shipra sinha</h5>
							<hr>
							<h6>Front End Developer</h6>
						</div>
					</div></a>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/team3.jpg" alt="">
						<div class="team-info">
							<h5>ankit pandey</h5>
							<hr>
							<h6>COO</h6>
						</div>
					</div></a>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/teamg.jpg" alt="">
						<div class="team-info">
							<h5>ishaanee pandey</h5>
							<hr>
							<h6>Administration</h6>
						</div>
					</div></a>
				</div>     
			</div>


			<div class="pure-g">
				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/team2.jpg" alt="">
						<div class="team-info">
							<h5>deepak chowdhary</h5>
							<hr>
							<h6>iOS Developer</h6>
						</div>
					</div></a>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/team1.jpg" alt="">
						<div class="team-info">
							<h5>tushar bose</h5>
							<hr>
							<h6>UI Designer</h6>
						</div>
					</div></a>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/teamg.jpg" alt="">
						<div class="team-info">
							<h5>paritosh kumar</h5>
							<hr>
							<h6>Backend Developer</h6>
						</div>
					</div></a>
				</div>

				<div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-2 pure-u-lg-1-4 team-child">
					<a href=""><div class="l-box">
						<img src="assets/img/team3.jpg" alt="">
						<div class="team-info">
							<h5>abhishek kumar</h5>
							<hr>
							<h6>Backend Developer</h6>
						</div>
					</div></a>
				</div>     
			</div>
		</div>
	</div>
</section> -->
<!-- ================ TEAM ends ================= -->


<!-- ================ CLIENTS ================= -->
<section>
	<div class="clients">
		<div class="container">
			<h4>Previous Work</h4>
			<div class="clients-section">
				<div class="outer_pad">
					<div class="left-arrow"><a id="btn_next1" href="#"><img src="assets/img/left-arrow.png" alt=""></a></div>
					<div class="choose_slider">
						<div class="animated_slider">
							<blockquote>
								<ul id="mySlider1">

									<li class="current_item">
										<div class="client-block">
											<img src="assets/img/allstarz_logo.png" alt="">
											<h5>All Starz</h5>
											<!-- 	<img src="assets/img/allstarz_logo.png" alt=""> -->
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod iure cum in, maxime ratione doloribus dolore corporis, eaque quisquam! Velit atque, est aliquid facilis perspiciatis. Quia ullam, numquam sed iste.</p>
											<a href="#">view case study</a>
										</div>
									<!-- 	<div class="client-img">
										<img src="assets/img/teamg2.jpg" alt="">
									</div> -->
								</li>

								<li class="current_item ">
									<div class="client-block">
										<img src="assets/img/allstarz_logo.png" alt="">
										<h5>Harrison Ford, CEO</h5>
										<!-- <img src="assets/img/allstarz_logo.png" alt=""> -->
										<p>Lorem ierspiciatis. Quia ullam, numquam sed iste.</p>
										<a href="#">view case study</a>
									</div>
										<!-- <div class="client-img">
											<img src="assets/img/teamg.jpg" alt="">
										</div> -->
									</li>

									<li class="current_item">
										<div class="client-block">
											<img src="assets/img/allstarz_logo.png" alt="">
											<h5>Harrison Ford, CEO</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod iure cum in, maxime ratione doloribus dolore corporis, eaque quisquam! Velit atque, est aliquid facilis perspiciatis. Quia ullam, numquam sed iste.</p>
											<a href="#">view case study</a>
										</div>
									<!-- 	<div class="client-img">
										<img src="assets/img/team1.jpg" alt="">
									</div> -->
								</li>

								<li class="current_item">
									<div class="client-block">
										<img src="assets/img/allstarz_logo.png" alt="">
										<h5>Harrison Ford, CEO</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod iure cum in, maxime ratione doloribus dolore corporis, eaque quisquam! Velit atque, est aliquid facilis perspiciatis. Quia ullam, numquam sed iste.</p>
										<a href="#">view case study</a>
									</div>
									<!-- 	<div class="client-img">
										<img src="assets/img/team3.jpg" alt="">
									</div> -->
								</li>

							</ul>
						</blockquote>
					</div>
				</div>
				<div class="right-arrow"><a id="btn_prev1" href="#"><img src="assets/img/right-arrow.png" alt=""></a></div>
			</div>
		</div>
	</div>
</div>
</section>

<div id="owl-example" class="owl-carousel clients">
	<div class="owl-slide">
		<div class="owl--text">
			<div class="client-block">
				<img src="assets/img/allstarz_logo.png" alt="">
				<h5>All Starz</h5>
				<!-- 	<img src="assets/img/allstarz_logo.png" alt=""> -->
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod iure cum in, maxime ratione doloribus dolore corporis, eaque quisquam! Velit atque, est aliquid facilis perspiciatis. Quia ullam, numquam sed iste.</p>
				<a href="#">view case study</a>
			</div>
		</div>
	</div>
	<div class="owl-slide">
		<div class="owl--text">
			<div class="client-block">
				<img src="assets/img/allstarz_logo.png" alt="">
				<h5>All Starz</h5>
				<!-- 	<img src="assets/img/allstarz_logo.png" alt=""> -->
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod iure cum in, maxime ratione doloribus dolore corporis, eaque quisquam! Velit atque, est aliquid facilis perspiciatis. Quia ullam, numquam sed iste.</p>
				<a href="#">view case study</a>
			</div>
		</div>
	</div>
	<div class="owl-slide">
		<div class="owl--text">
			<div class="client-block">
				<img src="assets/img/allstarz_logo.png" alt="">
				<h5>All Starz</h5>
				<!-- 	<img src="assets/img/allstarz_logo.png" alt=""> -->
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod iure cum in, maxime ratione doloribus dolore corporis, eaque quisquam! Velit atque, est aliquid facilis perspiciatis. Quia ullam, numquam sed iste.</p>
				<a href="#">view case study</a>
			</div>
		</div>
	</div>
</div>
<!-- ================ CLIENTS ends================= -->

<!-- ================ CONTACT US ================= -->
<section style="position: relative;">
	<div id="contact-us">
		<div class="container">
			<h4>Contact us</h4>
			<div class="pure-g">
				<div class="pure-u-md-1-5 hide-sm"></div>
				<div class="pure-u-md-3-5 pure-u-sm-1 contact-box">
					<form id="form_id" class="pure-form pure-form-stacked">
						<fieldset>
							<div class="pure-g">

								<div class="pure-u-1 pure-u-md-1-2 pure-u-sm-1">
									<input id="name" class="pure-u-23-24" type="text" placeholder="Your Name">
								</div>
								<div class="pure-u-1 pure-u-md-1-2  pure-u-sm-1">
									<input id="email" class="pure-u-1" type="email"  placeholder="Your Email Address" required>
								</div> 

									<!-- <div class="contact-left">
										<input id="name" class="" type="text" placeholder="Your Name">
									</div>
									
									<div class="contact-right">
										<input id="email" class="" type="email"  placeholder="Your Email Address" required>
									</div> -->
								</div>

								<div class="pure-g">
									<div class="pure-u-1 pure-u-md-1-2 pure-u-sm-1">
										<select id="state" class="pure-u-23-24 selectpicker">
											<option data-hidden="true">Budget</option>
											<option>CA</option>
											<option>IL</option>
										</select>
									</div>
							<!-- 	<div class="pure-u-1 pure-u-md-1-3 pure-u-sm-1">
								<input id="city" class="pure-u-23-24" type="text"  placeholder="Cost Calculator">
							</div> -->
							<div class="pure-u-1 pure-u-md-1-2 pure-u-sm-1">
								<div class="wrapper">
									<div class="drop">
										<div class="cont">
											<i class="fa fa-cloud-upload"></i>
											<div class="browse">
												<h3>Attach File</h3>
											</div>
										</div>
										<output id="list"></output><input id="files" multiple="true" name="files[]" type="file" />
									</div>
								</div>
							</div>
						</div>
						<div class="pure-g">
							<textarea class="pure-input-1" placeholder="Write something about your business..."></textarea>
						</div>
						<input type="checkbox" id="checkbox-1-1" class="ui-checkbox" checked/><label for="checkbox-1-1">Download and email NDA</label>
						<div class="submit-prt">
							<button type="submit" class="pure-button pure-button-primary">Submit</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="line-dot"></div>
<hr class="contact-hr">
</section>
<!-- ================ CONTACT US ends ================= -->

<!-- ============ FOOTER ============ -->
<footer>
	<div class="container">
		<div class="pure-g footer-sec">
			<div class="pure-u-lg-1 pure-u-md-1 pure-u-sm-1">
				<div class="pure-u-lg-1-4 pure-u-md-1-4 pure-u-sm-1 logo-sec">
					<a href=""><img src="assets/img/bw_logo.png" alt=""></a>
					<a href=""><h3>BOLDWARE</h3></a>
					<h4>Your Design Solution</h4>
				</div>

				<div class="pure-u-lg-1-4 pure-u-md-1-4 pure-u-sm-1 address">
					<div class="address1">
						<i class="fa fa-home" aria-hidden="true"></i>
						<p>Bold Kiln 3rd Floor,<br>D-58, Sector 10,<br>Noida, Uttar Pradesh 201303</p>
					</div>
				</div>

				<div class="pure-u-lg-1-4 pure-u-md-1-4 pure-u-sm-1 address">
					<div class="phone">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<p>+91 4584 485 364</p>
					</div>

						<div class="phone">
						<a href="mailto:ankit@boldware.co?Subject=Boldware" target="_top" ><i class="fa fa-envelope" aria-hidden="true"></i>ankit@boldware.co</a>
					</div>
				</div>

				<div class="pure-u-lg-1-4 pure-u-md-1-4 pure-u-sm-1 skype address">
					<a href="skype:-ankit@boldware.co-?chat" class="skype"><i class="fa fa-skype" aria-hidden="true"></i>ankit@boldware.co</a>
				</div>
				<!-- <div class="pure-u-lg-1-6 pure-u-sm-1-6 info">
					<ul>
						<li>information</li>
						<li><a href="">about us</a></li>
						<li><a href="">portfolio</a></li>
						<li><a href="">blog</a></li>
					</ul>
				</div>
				
				<div class="pure-u-lg-1-6 pure-u-sm-1-6 touch">
					<ul>
						<li>get in touch</li>
						<li><a href="">contact us</a></li>
						<li><a href="">careers</a></li>
						<li><a href="">get support</a></li>
					</ul>
				</div>
				
				<div class="pure-u-lg-1-6 pure-u-sm-1-6 legal">
					<ul>
						<li>legal</li>
						<li><a href="">privacy policy</a></li>
						<li><a href="">terms and conditions</a></li>
					</ul>
				</div> -->
			</div>

			<!-- <div class="pure-u-lg-1-4 pure-u-md-1-1 pure-u-sm-1-2 advice">
				<h2>get expert advice</h2>
				<form>
					<input type="email" placeholder="Your email address">
					<button type="submit"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
				</form>
				<h2>follow us</h2>
				<a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-behance" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			</div> -->
		</div>
<!-- 		<div class="copyright">
	<h4>copyright bold kiln services LLP 2015</h4>
</div> -->
</div>
</footer>
<!-- ============ FOOTER ends ============ -->
<script>
$("#form_id").submit(function(event) {
				event.preventDefault();
				var m_data = new FormData();    
				m_data.append( 'name', $('#name').val());								
				m_data.append( 'email', $('#email').val());
				m_data.append( 'desc', $('#desc').val());
				m_data.append( 'state', $('#state').val());
				m_data.append('check', $('input[name="check"]:checked').val());
				m_data.append( 'files', $('input[name=files]')[0].files[0]);
				
			        $.ajax({
					url      : "mail.php",
					data: m_data,
			              	processData: false,
			              	contentType: false,
			              	type: 'POST',
			              	dataType:'text',
			              	success: function(data){
			              		$( "#status" ).removeClass( "hidden" );
						$( "#status" ).html( data );	
						$('#gif').css('display', 'none');			

					}
				});
			});
			
	 function exefunction(x){
                if (document.getElementById('check').checked) 
                {
                	window.location = x;
                }
            }	

	/* ============ clients slider ============ */
	$(function() {
		$("#mySlider1").AnimatedSlider( { prevButton: "#btn_prev1", 
			nextButton: "#btn_next1",
			visibleItems: 3,
			infiniteScroll: true,
			willChangeCallback: function(obj, item) { $("#statusText").text("Will change to " + item); },
			changedCallback: function(obj, item) { $("#statusText").text("Changed to " + item); }
		});
	});

	$("#owl-example").owlCarousel({
		navigation : true, 
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem: true,
		pagination: false,
		rewindSpeed: 500
	});
</script>
</body>
</html>